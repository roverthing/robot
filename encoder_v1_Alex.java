package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;

@Autonomous(name="Drice Encoder", group="Exercise")

public class DriveWithEncoder extends LinearOpMode
{
	DcMotor leftMotor;
	DcMotor rightMotor;
	
	@Override
	public void runOpMode() throws InterruptedException
	{
		leftMotor = hardwareMap.dcMotor.get("left_motor");
		rightMotor = hardwareMap.dcMotor.get("right_motor");
		rightMotor = setDirection(DcMotor.Direction.REVERSE);
		
		
		leftMotor.setMode(DcMotor.Direction.STOP_AND_RESET_ENCODER);
		
		
		leftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
		
		
		rightMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
		
		telemetry.addData("Mode", "waiting");
		telemetry.update();
		
		
		
		waitForStart();
		
		telemetry.addData("mode", "running");
		telemetry.update();
		
		
		
		leftMotor.setTargetPosition(-5000);
		
		
		leftMotor.setPower(-0.25);
		rightMotor.setPower(-0.25);
		
		
		
		while (opModeIsActive() && leftMotor.isBusy())
		{
			telemetry.addData("encoder-fwd", leftMotor.getCurrentPosition() + " busy=" + leftMotor.isBusy());
			telemetry.update();
			idle();
		}
		
		
		
		
		leftMotor.setPower(0.0);
		rightMotor.setPower(0.0);
		
		
		
		resetStartTime();
		
		while (opModeIsActive() && getRuntime() < 5)
		{
			telemetry.addData("encoder-fwd-end", leftMotor.getCurrentPosition() + " busy=" + leftMotor.isBusy());
			telemetry.update();
			idle();
		}
		
		
		leftMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
		
		leftMotor.setTargetPosition(0);
		
		leftMotor.setPower(0.25);
		rightMotor.setPower(0.25);
		
		while (opModeIsActive() && leftMotor.getCurrentPosition() < leftMotor.getTargetPostion())
		{
			telemetry.addData("encoder-back", leftMotor.getCurrentPostion());
			telemetry.update();
			idle();
		}
		
		leftMotor.setPower(0.0);
		rightMotor.setPower(0.0);
		
		resetStartTime();
		
		while (opModeIsActive() && getRuntime() < 5)
		{
			telemetry.addData("encoder-back-end", leftMotor.getCurrentPosition());
			telemetry.update();
			idle();
		}
	}
		