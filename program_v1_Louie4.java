package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
@TeleOp
public class Test9110 extends LinearOpMode {
	DcMotor DriveFL; // front left weeel
	DcMotor DriveFR; // front right weel
	DcMotor DriveBL; // back left weel
	DcMotor DriveBR; // back right weel
	DcMotor ArmB;  // base of arm up
	DcMotor ArmB2;  // base of arm up
	DcMotor ArmT;  // top of arm up
	DcMotor ArmT2;  // top of arm up
	@Override
	public void runOpMode() throws InterruptedException{
		//left joystick moves robot
		//right joystick moves arm, y axis for base and x asis for top
		DriveFL = hardwareMap.dcMotor.get("DriveFL");
		DriveFR = hardwareMap.dcMotor.get("DriveFR");
		DriveBL = hardwareMap.dcMotor.get("DriveBL");
		DriveBR = hardwareMap.dcMotor.get("DriveBR");
      /*  ArmT = hardwareMap.dcMotor.get("ArmT");
        ArmT2 = hardwareMap.dcMotor.get("ArmT2");
        ArmB = hardwareMap.dcMotor.get("ArmBL");
        ArmB2 = hardwareMap.dcMotor.get("ArmB2");
        */
		DriveFR.setDirection(DcMotorSimple.Direction.REVERSE);
		DriveBR.setDirection(DcMotorSimple.Direction.REVERSE);
		while(!(isStarted()  || isStopRequested())) {
			idle();
		}
		waitForStart();
		while(opModeIsActive()) {
			DriveFL.setPower(gamepad1.left_stick_y);
			DriveFR.setPower(gamepad1.left_stick_y);
			DriveBR.setPower(gamepad1.left_stick_y);
			DriveBL.setPower(gamepad1.left_stick_y);
			if((gamepad1.left_stick_x)<0){//turn left
				DriveFL.setPower(-gamepad1.left_stick_x);
				DriveFR.setPower(gamepad1.left_stick_x);
				DriveBR.setPower(gamepad1.left_stick_x);
				DriveBL.setPower(-gamepad1.left_stick_x);
			}
			if((gamepad1.left_stick_x)>0){//turn right
				DriveFL.setPower(gamepad1.left_stick_x);
				DriveFR.setPower(-gamepad1.left_stick_x);
				DriveBR.setPower(-gamepad1.left_stick_x);
				DriveBL.setPower(gamepad1.left_stick_x);
			}
         /* ArmT.setPower(gamepad1.right_stick_y);
            ArmT2.setPower(gamepad1.right_stick_y);
            ArmB.setPower(gamepad1.right_stick_x);
            ArmB2.setPower(gamepad1.right_stick_x);
        */

		}
	}
}