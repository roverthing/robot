package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
@TeleOp
public class Group6 extends LinearOpMode {
	DcMotor DriveLeft;
	DcMotor DriveRight;
	DcMotor DriveF;
	DcMotor DriveB;
	public void runOpMode(){
		DriveLeft = hardwareMap.dcMotor.get("DriveLeft");
		DriveRight = hardwareMap.dcMotor.get("DriveRight");
		DriveF = hardwareMap.dcMotor.get("DriveF");
		DriveB = hardwareMap.dcMotor.get("DriveB");
		DriveRight.setDirection(DcMotorSimple.Direction.REVERSE);
		while(opModeIsActive()){
			DriveLeft.setPower(gamepad1.left_stick_y);
			DriveRight.setPower(gamepad1.right_stick_y);
			DriveF.setPower(gamepad1.left_stick_y);
			DriveB.setPower(gamepad1.right_stick_y);
		}
	}
}