package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
@TeleOp
public class Group6 extends LinearOpMode {
	DcMotor DriveFL; // front left weeel
	DcMotor DriveFR; // front right weel
	DcMotor DriveBL; // back left weel
	DcMotor DriveBR; // back right weel
	DcMotor ArmB;  // base of arm up
	DcMotor ArmT;  // top of arm up
    DcMotor ArmT2;  // top of arm up
	public void runOpMode(){
		//left joystick moves robot
		//right joystick moves arm, y axis for base and x asis for top
		DriveFL = hardwareMap.dcMotor.get("DriveFL");
		DriveFR = hardwareMap.dcMotor.get("DriveFR");
		DriveBL = hardwareMap.dcMotor.get("DriveBL");
		DriveBR = hardwareMap.dcMotor.get("DriveBR");
		ArmT = hardwareMap.dcMotor.get("ArmT");
		ArmT2 = hardwareMap.dcMotor.get("ArmT2");
		ArmB = hardwareMap.dcMotor.get("ArmB");
		DriveFR.setDirection(DcMotorSimple.Direction.REVERSE);
		DriveBR.setDirection(DcMotorSimple.Direction.REVERSE);
		ArmT2.setDirection(DcMotorSimple.Direction.REVERSE);
		while(opModeIsActive()) {
			DriveFL.setPower(gamepad1.left_stick_x);
			DriveFR.setPower(gamepad1.left_stick_x);
			DriveBF.setPower(gamepad1.left_stick_y);
			DriveBL.setPower(gamepad1.left_stick_y);
			ArmT.setPower(gamepad1.right_stick_y);
			ArmT2.setPower(gamepad1.right_stick_y);
			ArmB.setPower(gamepad1.right_stick_x);
		}
	}
}