package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
@TeleOp
public class Tel extends LinearOpMode {
	DcMotor FL; // front left weeel
	DcMotor FR; // front right weel
	DcMotor BL; // back left weel
	DcMotor BR; // back right weel
	DcMotor ArmB;  // base of arm up
	DcMotor ArmB2;  // base of arm up
	DcMotor ArmT;  // top of arm up
	DcMotor ArmT2;  // top of arm up
	@Override
	public void runOpMode() throws InterruptedException{
		//left joystick moves robot
		//right joystick moves arm, y axis for base and x asis for top
		FL = hardwareMap.dcMotor.get("FL");
		FR = hardwareMap.dcMotor.get("FR");
		BL = hardwareMap.dcMotor.get("BL");
		BR = hardwareMap.dcMotor.get("BR");
		ArmT = hardwareMap.dcMotor.get("ArmT");
		ArmT2 = hardwareMap.dcMotor.get("ArmT2");
		ArmB = hardwareMap.dcMotor.get("ArmB");
		ArmB2 = hardwareMap.dcMotor.get("ArmB2");
		float LT = gamepad1.left_trigger;
		float RT = gamepad1.right_trigger;
		ArmB.setDirection(DcMotorSimple.Direction.REVERSE);
		ArmB2.setDirection(DcMotorSimple.Direction.REVERSE);
		FR.setDirection(DcMotorSimple.Direction.REVERSE);
		BR.setDirection(DcMotorSimple.Direction.REVERSE);
		while(!(isStarted()  || isStopRequested())) {
			idle();
		}
		waitForStart();
		while(opModeIsActive()) {
			if((gamepad1.left_stick_y)<0.2 || (gamepad1.left_stick_y)>0.2){
				FL.setPower(gamepad1.left_stick_y);
				FR.setPower(gamepad1.left_stick_y);
				BR.setPower(gamepad1.left_stick_y);
				BL.setPower(gamepad1.left_stick_y);}
			if((gamepad1.left_stick_x)<0.2){//turn left
				FL.setPower(-gamepad1.left_stick_x);
				FR.setPower(gamepad1.left_stick_x);
				BR.setPower(gamepad1.left_stick_x);
				BL.setPower(-gamepad1.left_stick_x);
			}
			if((gamepad1.left_stick_x)>0.2) {//turn right
				FL.setPower(gamepad1.left_stick_x);
				FR.setPower(-gamepad1.left_stick_x);
				BR.setPower(-gamepad1.left_stick_x);
				BL.setPower(gamepad1.left_stick_x);
			}
			if((gamepad1.right_stick_y)>0.2) {
				ArmT.setPower(gamepad1.right_stick_y);
				ArmT2.setPower(gamepad1.right_stick_y);}
			if((gamepad1.right_stick_x)>0.2) {
				ArmB.setPower(gamepad1.right_stick_x);
				ArmB2.setPower(gamepad1.right_stick_x);}

		}
	}
}