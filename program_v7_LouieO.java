package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
@TeleOp
public class Tel extends LinearOpMode {
    DcMotor FL; // front left weeel
    DcMotor FR; // front right weel
    DcMotor BL; // back left weel
    DcMotor BR; // back right weel
    DcMotor ArmB;  // base of arm up
    DcMotor ArmB2;  // base of arm up
    DcMotor ArmB3;  //baser of arm up (the third one)
    DcMotor ArmT;  // top of arm up
    //DcMotor ArmT2;  // top of arm up
     float LT;  //  left trigger
     float RT; // right trigger
    @Override
    public void runOpMode() throws InterruptedException{
        //left joystick moves robot
        //right joystick moves arm, y axis for base and x asis for top
        FL = hardwareMap.dcMotor.get("FL");
        FR = hardwareMap.dcMotor.get("FR");
        BL = hardwareMap.dcMotor.get("BL");
        BR = hardwareMap.dcMotor.get("BR");
        ArmT = hardwareMap.dcMotor.get("ArmT");
        //ArmT2 = hardwareMap.dcMotor.get("ArmT2");
        ArmB = hardwareMap.dcMotor.get("ArmB");
        ArmB3 = hardwareMap.dcMotor.get("ArmB3");
        ArmB2 = hardwareMap.dcMotor.get("ArmB2");
        LT = gamepad1.left_trigger;
        RT = gamepad1.right_trigger;
        ArmB.setDirection(DcMotorSimple.Direction.REVERSE);
        ArmB2.setDirection(DcMotorSimple.Direction.REVERSE);
        FL.setDirection(DcMotorSimple.Direction.REVERSE);
        BL.setDirection(DcMotorSimple.Direction.REVERSE);
        while(!(isStarted()  || isStopRequested())) {
            idle();
        }
        waitForStart();
        while(opModeIsActive()) {
	int Armstr=0;	
		Boolean y = gamepad1.y;
		if(y==true){
		Armstr=1.0;
}		else{
		Armstr=0.3;
}           if((gamepad1.left_stick_x)<-0.6 ){//turn left
                FL.setPower(0.5);
                FR.setPower(-0.5);
                BR.setPower(-0.5);
                BL.setPower(0.5);
            }
            else if((gamepad1.left_stick_x)>0.6) {//turn right
                FL.setPower(-0.5);
                FR.setPower(0.5);
                BR.setPower(0.5);
                BL.setPower(-0.5);
            }else {//move forward / backwords
                FL.setPower(gamepad1.left_stick_y);
                FR.setPower(gamepad1.left_stick_y);
                BR.setPower(gamepad1.left_stick_y);
                BL.setPower(gamepad1.left_stick_y);
            }
            if((gamepad1.right_trigger)>0.1 ) {
                ArmT.setPower(Armstr);
                }
                else if ((gamepad1.left_trigger)>0.1){
                ArmT.setPower(-Armstr);
                }else{
                 ArmT.setPower(0);  
                }
            if(gamepad1.right_bumper==true ) {
                ArmB.setPower(Armstr);
                ArmB2.setPower(Armstr);
                ArmB3.setPower(Armstr);
            }else if (gamepad1.left_bumper==true){
                ArmB.setPower(-Armstr);
                ArmB2.setPower(-Armstr); 
                ArmB3.setPower(-Armstr);
                }else{
                ArmB.setPower(0);
                ArmB2.setPower(0);  
                }
        }
    }
}